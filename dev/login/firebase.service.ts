import {Injectable} from 'angular2/core';
import {Http} from 'angular2/http';
import {Router, ROUTER_DIRECTIVES} from "angular2/router";

@Injectable()
export class FirebaseService {
    
    appUrl: string = "https://blazing-inferno-9370.firebaseio.com/";

    firebase = new Firebase("https://blazing-inferno-9370.firebaseio.com/");
   
    

        
        login(email: string, password: string, callback) {
        this.firebase.authWithPassword({
            email: email,
            password: password
        }, function(error, data) {
            if (error) {
                callback(false)
            } else {
                return callback(data.token);
            }
            
        })
    }   
}