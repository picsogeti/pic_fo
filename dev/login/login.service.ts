import {Injectable} from "angular2/core";
import {Http, Headers} from "angular2/http";
import { Router, RouterLink } from 'angular2/router';
import 'rxjs/add/operator/map';


@Injectable()
        // Service d authentification de l utilisateur

export class LoginService{
    
    
 
    constructor (public _http: Http, public router: Router) {}
    
    login(username:string,password:string){
                
           
        
      
        //  Les variables dans envoyées dans un fichier json
        let json = JSON.stringify({username, password});
        let headers = new Headers();
       
       // definition du headers
       
       headers.append('Content-Type', 'application/json');
       headers.append('Access-Control-Allow-Origin', '*');
      
            //  apple de l api pour la verification des identifiants
    
        this._http.post('http://localhost:8080/PIC_BO/PIC_BO/authentification/auth ',json,{ headers: headers })
             .subscribe(
                response => {       // jeton receptionné si l authentification est verifié et mis dans le local storage
                   let jwt =response.json(); 
                  localStorage.setItem('tokenMembre',JSON.stringify(jwt));
                   this.router.parent.navigateByUrl('/Home');                                                      
                },
                Error  => {
                     
                    alert(Error .text());   // traite exeption sur la connexion
                                                     
                },
                () => console.log('completed')
            
             )
    }
}
