//Angular
import {Component} from "angular2/core";
import {Router, ROUTER_DIRECTIVES} from "angular2/router";
import { CORE_DIRECTIVES, FORM_DIRECTIVES } from 'angular2/common';
import {TranslateService, TranslatePipe} from 'ng2-translate/ng2-translate';


//Service
//import {FirebaseService} from "./firebase.service";
import {LoginService} from "./login.service";

@Component({
    selector:'login',
    providers:[LoginService],
    templateUrl: './dev/login/login.html',
    directives: [CORE_DIRECTIVES,FORM_DIRECTIVES, ROUTER_DIRECTIVES],
    pipes: [TranslatePipe]
    
})

export class LoginComponent {
    
   public error = false;
       errorMsg : string;
    constructor (public _loginService : LoginService, private _router: Router){}
    
    // identification utilisateur
    
    login(username, password) {
                
     this._loginService.login(username, password);
      localStorage.setItem('username',username);
           
      
    }  
}
