import {Component} from "angular2/core";

@Component({
    selector:'Import-projet',
    template:`
    <div class="container margtop">
        <h2 class="text-center">
            <span class="glyph glyphicon glyphicon-folder-open colorTextSogeti"></span>
            <b> Import Project</b>
        </h2>
        <h1>En construction...</h1>
    </div>
    `
})

export class ImportProjetComponent {
        
}