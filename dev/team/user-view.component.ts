import {Component, OnInit} from "angular2/core";
import {RouteParams, Router} from 'angular2/router';
import { CORE_DIRECTIVES, FORM_DIRECTIVES } from 'angular2/common';
import {Roles} from './roles';

//Service
import {UserService} from "./user.service";
import {User} from './user';

@Component({
    selector: "user-view",
    templateUrl: "/dev/team/user-view.component.html",
    directives: [CORE_DIRECTIVES],
    providers: [UserService],
    
})

export class UserViewComponent implements OnInit {
    user: User[];
      roles_list: Roles[];

    constructor(private _userService: UserService, private _routeParams: RouteParams,private _router: Router) {
        
        this._userService=_userService;
        this._routeParams=_routeParams;
     }



    deleteUser(user: User) {
        this._userService.deleteUser(user.idMembre);
        this._router.parent.navigateByUrl('/Home/Team');
        return false;
        
    }
    SetUser(username, role) {
        let idMembre = this._routeParams.get('idMembre');
        this._userService.setUser(idMembre, username, role);
        
        this._router.parent.navigateByUrl('/Home/Team');
       return false;
    }
   
    ngOnInit(user: User) {
        
        let idMembre = this._routeParams.get('idMembre');
        console.log(idMembre);
         console.log("user = "+this.user);
        
      
       
        
        
        
       // console.log(user.username);
    /* this._userService.getUser(idMembre)
            .subscribe(
            data => this.user = data,
            error => console.log(error)
            )*/
    }
}