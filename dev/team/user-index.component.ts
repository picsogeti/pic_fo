import {Component, OnInit} from "angular2/core";
import {ROUTER_DIRECTIVES, Router} from 'angular2/router';
import {TranslatePipe, TranslateService} from 'ng2-translate/ng2-translate';
import { CORE_DIRECTIVES, FORM_DIRECTIVES } from 'angular2/common';

//Service
import {UserService} from "./user.service";

// interface
import {User} from './user';

@Component({
    selector: 'user-index',
    templateUrl: '/dev/team/user-index.component.html',
    directives: [ROUTER_DIRECTIVES, FORM_DIRECTIVES, CORE_DIRECTIVES],
    pipes: [TranslatePipe],     // filtre pour la traduction
    providers: [UserService]

})

export class UserIndexComponent implements OnInit {
    users_list: User[];
    user_select:User[];
     view = false;
  
    constructor(private _userService: UserService, private _router: Router) {}    
    
    viewUser(user: User){       
        console.log(user);         
        this._router.navigate(["UserView", { idMembre: user.idMembre}]);
       
    }
    
    deleteUser(user: User){
        this._userService.deleteUser(user.idMembre);
        this._router.parent.navigateByUrl('/Home/Team');
        //location.reload();
    }

    ngOnInit() {     
        this._userService.getUsers()
            .subscribe(
            Team_List => this.users_list = Team_List,
            error => console.log(error)
            );
            
       }
}