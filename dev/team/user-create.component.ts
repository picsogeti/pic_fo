import {Component,  OnInit} from "angular2/core";
import {Router} from 'angular2/router';
import {Roles} from './roles';
import { CORE_DIRECTIVES, FORM_DIRECTIVES } from 'angular2/common';

//Service
import {UserService} from "./user.service";

@Component({
    selector: 'new-projet',
    templateUrl: './dev/team/user-create.component.html',
     directives: [ FORM_DIRECTIVES, CORE_DIRECTIVES],
    providers: [UserService]

})

export class UserCreateComponent  implements OnInit {
    roles_list: Roles[];

    constructor(private _userService: UserService, private _router: Router) { }

    CreateUser(username, idRole, password) {
     let self = this;

        this._userService.createUser(username, idRole, password)
            .subscribe(                               
                this._router.parent.navigateByUrl('/Home/Team')                                 
                )
console.log ('page team'); 
return false;
//location.reload();
  }
  
   ngOnInit() {
       
      this._userService.getRole()
      .subscribe(
            Roles_List => this.roles_list = Roles_List,
             console.log("role list" + this.roles_list),
            error => console.log(error)
           )  ;
    
}
        
        
}