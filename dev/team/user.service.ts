import {Injectable, Inject} from 'angular2/core';
import {Http, Headers} from 'angular2/http';
import {Observable} from 'rxjs/Observable';

//import {FirebaseService} from "../login/firebase.service";

import {User} from './user';
import {Roles} from './roles';

@Injectable()


export class UserService {
    
  //  serveur = new serveur("http://10.226.166.15:8080/");
tokenMembre: string;
  constructor( private _http: Http) {
      
     this.tokenMembre = localStorage.getItem('tokenMembre');
      
  }
    
    
    createUser(username: string, idRole: any, password: string) {
        //console.log('function create');
        const body = JSON.stringify({ username, idRole, password});
        let headers = new Headers();
        
       
       headers.append('Content-Type', 'application/json');
       headers.append('Access-Control-Allow-Origin', '*');    
       headers.append('Authorization', 'Bearer : ' + this.tokenMembre);  
       console.log("json token"+ localStorage.getItem('tokenMembre')); 
       console.log("headers =" + JSON.stringify(headers));
     
      

        return this._http.post('http://localhost:8080/PIC_BO/PIC_BO/membre/membre', body,{ headers: headers })
              .map(response =>{response.json()
                  return response })
    }
 
        // recupére la liste des roles possible
  getRole(){
      
         return this._http.get<Roles[]> ('http://localhost:8080/PIC_BO/PIC_BO/role/roles')
            .map(res => {                             
                let roles =res.json(); 
                 console.log('roles',JSON.stringify(roles))
                 let result: Array<Roles> = [];
                Object.keys(roles).forEach(function (key) {
                    let postObject: Roles;
                   postObject = { idRole :roles[key].idRole, codeRole: roles[key].codeRole, libelleRole: roles[key].libelleRole};             
                    result.push(postObject);  })
                return result                
            }
                 ); 
  }
    
    //  methode pour appeler la liste des membres
    
    getUsers(): Observable<User[]> {
        return this._http.get('http://localhost:8080/PIC_BO/PIC_BO/membre/membres')
            .map(res => {
                let data = res.json();                
                let result: Array<User> = [];
                Object.keys(data).forEach(function (key) {
                    let postObject: User;
                   postObject = { idMembre :data[key].idMembre, username: data[key].username, idRole: data[key].idRole,status: data[key].status };             
                    result.push(postObject);  })
                return result
            })
    }
    
    
        getUser(idMembre: string){
                        
        }
    
 /*   getUser(idMembre: string): Observable<User>{
      //  let url: string;
         let headers = new Headers();
       
       headers.append('Content-Type', 'application/json');
       headers.append('Access-Control-Allow-Origin', '*');
       console.log("getUser");
       
 return this._http.get("http://10.226.166.24:8080/PIC_BO/PIC_BO/membre/membre/" + idMembre,{ headers: headers })
            .map(response => response.json());
        
    }*/
    deleteUser(idMembre: string) {
    //    this.firebase.child('user').child(id).remove();
        
          let headers = new Headers();
       
       headers.append('Content-Type', 'application/json');
       headers.append('Access-Control-Allow-Origin', '*');
         headers.append('Authorization', 'Bearer : ' + this.tokenMembre);  
       
         
   this._http.delete('http://localhost:8080/PIC_BO/PIC_BO/membre/membre/' + idMembre,{ headers: headers })
            .subscribe((res) => {});                    
    }
    
    
     // Modification dun membre
     setUser(idMembre: string, username: string, role: string) {
     //   this.firebase.child('user').child(idMembre).set({ username: username, role: role });
     
        const body = JSON.stringify({idMembre,username, role});
        let headers = new Headers();
       
       headers.append('Content-Type', 'application/json');
       headers.append('Access-Control-Allow-Origin', '*');
       headers.append('Authorization', 'Bearer : ' + this.tokenMembre);  
         console.log("service setuser" +body);
         
    return this._http.put('http://localhost:8080/PIC_BO/PIC_BO/membre/Membre', body,{ headers: headers })
            .subscribe(response => {
            })
    }
    
     
}