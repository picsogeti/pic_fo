import {Injectable, Inject} from 'angular2/core';
import {Http, Headers} from 'angular2/http';
import {Observable} from 'rxjs/Observable';

import {FirebaseService} from "../login/firebase.service";

import {Project} from './project';

@Injectable()

export class ProjectService {
    private _postUrl;
    tokenMembre: string;
   firebase = new Firebase("https://blazing-inferno-9370.firebaseio.com/");

    constructor( private _http: Http) {
           this.tokenMembre = localStorage.getItem('tokenMembre');

    }
       
    Create(nomProjet: string, description: string, status:string, branche: string, frequence:string, credential:string, url :string ) {
        //console.log('function create');
        const body = JSON.stringify({ nomProjet: nomProjet, description: description,status:status, branche: branche, frequence:frequence, credential:credential, url:url});
        
        let headers = new Headers();
        
       
       headers.append('Content-Type', 'application/json');
       headers.append('Access-Control-Allow-Origin', '*');    
       headers.append('Authorization', 'Bearer : ' + this.tokenMembre);  
       console.log("json token ="+ localStorage.getItem('tokenMembre')); 
       console.log("headers =" + JSON.stringify(headers));
        console.log("body =" + JSON.stringify(body));
        

        return this._http.post('http://localhost:8080/PIC_BO/PIC_BO/projet/projet', body,{ headers: headers })
            .map(response => response.json())
            //let data = JSON.stringify(response)});
    }

    getProjects(): Observable<Project[]> {
            
             let headers = new Headers();
        
       
       headers.append('Content-Type', 'application/json');
       headers.append('Access-Control-Allow-Origin', '*');    
       headers.append('Authorization', 'Bearer : ' + this.tokenMembre);  
       console.log("json token ="+ localStorage.getItem('tokenMembre')); 
       console.log("headers =" + JSON.stringify(headers));
        
            
            
            
        return this._http.get('http://localhost:8080/PIC_BO/PIC_BO/projet/projets',{ headers: headers })
            .map(res => {
                let data = res.json();
                let result: Array<Project> = [];
                Object.keys(data).forEach(function (key) {
                    let postObject: Project;
                    postObject = { idProjet: data[key].idProjet, nomProjet: data[key].nomProjet, credential: data[key].credential, frequence: data[key].frequence,
                         branche: data[key].branche, description: data[key].description,status: data[key].status, url: data[key].url, idClient: data[key].idClient,
                          idRoleProjet: data[key].idRoleProjet,idMembre: data[key].idMembre, idRole: data[key].idRole  };
                    result.push(postObject);
                })
                return result
            })
    }

    getProject(id: string): Observable<Project> {
        let url: string;
        url = "https://blazing-inferno-9370.firebaseio.com/" + "project/" + id + ".json"
        return this._http.get(url)
            .map(response => response.json());
    }

    deleteProject(idProjet: string) {
        let headers = new Headers();
     //   this.firebase.child('project').child(id).remove();
       headers.append('Content-Type', 'application/json');
       headers.append('Access-Control-Allow-Origin', '*');
         headers.append('Authorization', 'Bearer : ' + this.tokenMembre);  
       
         
   this._http.delete('http://localhost:8080/PIC_BO/PIC_BO/projet/projet/' + idProjet,{ headers: headers })
            .subscribe((res) => {});                    
    }
       
    

    setProject(id: string, name: string, description: string, date: string) {
        this.firebase.child('project').child(id).set({ name: name, description: description, date: date });
    }

}