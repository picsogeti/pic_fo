export class Project{
    idProjet: any;
    nomProjet: string;
    credential : string;
    frequence : string;
    branche:string;
    status:string;
    url:string;
    idClient:any;
    description: string;
    idRoleProjet:any;
    idMembre:any;
    idRole:any;
}